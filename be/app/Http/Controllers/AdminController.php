<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use App\Models\Report;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->guard('admin')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function index(){
        $user = User::all();
        return response()->json($user);

        $report = Report::all();
        return response()->json($report);
    }

    public function update(Request $request) {
        try {
            $data = [
                'first_name' => $request->first_name,
                'gender' => $request->gender,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'contact' => $request->contact,
            ];

            if($request->password) {
                $data['password'] = Hash::make($request->password);
            }

            $book = Admin::find($request->id);

            $book->update($data);

           return response()->json(['message' => 'User updated successfully!'], 200);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'User not found'], 404);
        }
    }

    
    public function me()
    {
        return response()->json(auth('admin')->user());
        return response()->json(auth('api')->user());
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            "success" => true,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('admin')->factory()->getTTL() * 60,
            'user' => auth('admin')->user()
        ]);
    }
}
