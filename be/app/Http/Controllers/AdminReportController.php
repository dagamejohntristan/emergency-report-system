<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;

class AdminReportController extends Controller
{
    public function AdminReport(){
        $report = Report::all();
        return response()->json($report);
    }

    public function allReports(){
        $totalReports = Report::count();
        return response()->json($totalReports);
    
    }
    public function pendingReports(){
        $pendingReports = Report::where('report_status', 'Action needed')->count();
        return response()->json($pendingReports);
    }

    public function AdminDeleteReport($id){
        $report = Report::destroy($id);
        return response()->json($report);
    }

    public function respondReport(Request $request) {
        try {
            $update = [
                'report_status' => $request->report_status
            ];
            $user = Report::find($request->id);

            $user->update($update);

           return response()->json($user);
        } catch (ModelNotFoundException $exception) {
            return response()->json($user);
        }
    }
}
