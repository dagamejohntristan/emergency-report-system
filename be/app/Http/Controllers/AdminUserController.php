<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AdminUserController extends Controller
{
    public function AdminUser(){
        $user = User::all();
        return response()->json($user);
    }

    public function allAccounts(){
        $totalAccounts = User::count();
        return response()->json($totalAccounts);
    }

    public function AdminUpdateUser(Request $request) {
        try {
            $data = [
                'first_name' => $request->first_name,
                'gender' => $request->gender,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'contact' => $request->contact,
                'account_status' => $request->account_status,
            ];
            $user = User::find($request->id);

            $user->update($data);

           return response()->json($user);
        } catch (ModelNotFoundException $exception) {
            return response()->json($user);
        }
    }

    public function delete($id){
        $user = User::destroy($id);
        return response()->json($user);
    }

}
