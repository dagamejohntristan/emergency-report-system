import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/auth/Login.vue'
import Register from '@/components/auth/Register.vue'
import Dashboard from '@/components/admin/Dashboard.vue'
import Reports from '@/components/admin/Reports.vue'
import Users from '@/components/admin/Users.vue'
import UserDashboard from '@/components/user/UserDashboard.vue'
import UserProfile from '@/components/user/Profile.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
      routes: [
      {
        path: '/', 
        name: 'login',
        component: Login,
        meta: {
          hasUser: true
        }
      },
      {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
          hasUser: true
        }
      },
      {
        path: '/admin/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
          requiresLogin: true,
          isAdmin: true
        }
      },
      {
        path: '/admin/report',
        name: 'reports',
        component: Reports,
        meta: {
          requiresLogin: true,
          isAdmin: true
        }
      },
      {
        path: '/admin/user',
        name: 'users',
        component: Users,
        meta: {
          requiresLogin: true,
          isAdmin: true
        }
      },
      {
        path: '/user/dashboard',
        name: 'userdashboard',
        component: UserDashboard,
        meta: {
          isUser: true
        }
      },
      {
        path: '/user/profile',
        name: 'userprofile',
        component: UserProfile,
        meta: {
          isUser: true
        }
      }
    ]
    })

    router.beforeEach((to, from, next) => {
      if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
        next({name: 'login'})
      }
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
          next({ name: "dashboard" });
      } 
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
          next({ name: "userdashboard" });
      } 
      else if (to.matched.some((record) => record.meta.isAdmin) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
          next({ name: "userdashboard" });
      } 
      else if (to.matched.some((record) => record.meta.isUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
          next({ name: "dashboard" });
      } 
      else {
        next();
      }
    });

    export default router