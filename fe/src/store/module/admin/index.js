import API from '../../base/'

export default ({
 namespaced: true,
 state: {
    reports: [],
    users: [],
    total_users: [],
    total_reports: [],
    total_pending_reports: [],
 },
 mutations: {
    SET_REPORTS(state, data){
      state.reports = data
    },
    SET_USERS(state, data){
      state.users = data
    },
    SET_TOTAL_USERS(state, data){
      state.total_users = data
    },
    SET_TOTAL_REPORTS(state, data){
      state.total_reports = data
    },
    SET_PENDING_REPORTS(state, data){
      state.total_pending_reports = data
    },
    DELETE_REPORT(state, id){
        state.reports = state.reports.filter(reports => {
          return reports.id !== id;
        });
    },
    DELETE_USER(state, id){
        state.users = state.users.filter(users => {
          return users.id !== id;
        });
    },
  },
 actions: {
    async getReport({commit}){
      const res = await API.get('/auth/admin/reports').then(res => {
        commit('SET_REPORTS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getUser({commit}){
      const res = await API.get('/auth/admin/users').then(res => {
        commit('SET_USERS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getTotalUser({commit}){
      const res = await API.get('/auth/admin/total_users').then(res => {
        commit('SET_TOTAL_USERS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getTotalReport({commit}){
      const res = await API.get('/auth/admin/total_reports').then(res => {
        commit('SET_TOTAL_REPORTS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getPendingReport({commit}){
      const res = await API.get('/auth/admin/pending_reports').then(res => {
        commit('SET_PENDING_REPORTS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async updateUser({commit}, data){
        const res = await API.put('/auth/admin/user_update', data).then(res => {
            
          return res;
        }).catch(err => {
         return err.response;
        })
  
        return res;
      },

    async deleteReport({commit}, id){
      const res = await API.delete(`/auth/admin/delete_report/${id}`).then(res => {
        commit('DELETE_REPORT', id)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async deleteUser({commit}, id){
      const res = await API.delete(`/auth/admin/delete/${id}`).then(res => {
        commit('DELETE_USER', id)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async respondReport({commit}, data){
      const res = await API.put('/auth/admin/respond_report', data).then(res => {
          
        return res;
      }).catch(err => {
       return err.response;
      })
  
      return res;
    }
  },
  getters: {
  },
})