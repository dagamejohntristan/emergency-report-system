import API from '../../base/'

export default ({
 namespaced: true,
 state: {
    reports: [],
    users: [],
 },

 actions: {
    async createReport({commit}, data){
      const res = await API.post('/auth/report/store', data).then(res => {
        
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getReport({commit}){
      const res = await API.get('/auth/report/index').then(res => {
        commit('SET_REPORTS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getUser({commit}){
      const res = await API.get('/auth/admin/index').then(res => {
        commit('SET_USERS', res.data)
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async updateReport({commit}, data){
      const res = await API.put(`/auth/report/update/${data.id}`, data).then(res => {

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async deleteReport({commit}, id){
      const res = await API.delete(`/auth/report/delete/${id}`).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async deleteAdminReport({commit}, id){
      const res = await API.delete(`/auth/admin/delete/${id}`).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async deleteUser({commit}, id){
      const res = await API.delete(`/auth/admin/delete/${id}`).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    }
  },
  
  getters: {

  },
  mutations: {
    SET_REPORTS(state, data){
      state.reports = data
    },
    SET_USERS(state, data){
      state.users = data
    }
  }
})